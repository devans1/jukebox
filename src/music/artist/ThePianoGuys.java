package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class ThePianoGuys {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public ThePianoGuys() {
    }
    
    public ArrayList<Song> getThePianoGuysSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Winter Wind", "The Piano Guys");             		//Create a song
         Song track2 = new Song("Silent Night", "The Piano Guys");         		//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the The Piano Guys
         this.albumTracks.add(track2);                                          //Add the second song to song list for the The Piano Guys 
         return albumTracks;                                                    //Return the songs for the The Piano Guys in the form of an ArrayList
    }
}
