package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class GunsNRoses {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public GunsNRoses() {
    }
    
    public ArrayList<Song> getGunsNRosesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();									//Instantiate the album so we can populate it below
    	 Song track1 = new Song("Paradise City", "Guns N' Roses");          	//Create a song
         Song track2 = new Song("Sweet Child O' Mine", "Guns N' Roses");    	//Create another song
         Song track3 = new Song("Knockin' on Heavens's door", "Guns N Roses");  //Create another song
         this.albumTracks.add(track1);                                      	//Add the first song to song list for the Guns N' Roses
         this.albumTracks.add(track2); 											//Add the second song to song list for the Guns N' Roses 
         this.albumTracks.add(track3); 											//Add the third song to song list for the Guns N' Roses 
         return albumTracks;                                                	//Return the songs for the Guns N' Roses in the form of an ArrayList
    }
}
