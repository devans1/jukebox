package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class ChildishGambino {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public ChildishGambino() {
    }
    
    public ArrayList<Song> getChildishGambinoSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Freaks and Geeks", "Childish Gambino");        //Create a song
         Song track2 = new Song("Time", "Childish Gambino");         			//Create another song
         Song track3 = new Song("12.38", "Childish Gambino");         			//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Childish Gambino
         this.albumTracks.add(track2); 											//Add the second song to song list for Childish Gambino 
         this.albumTracks.add(track3); 											//Add the third song to song list for Childish Gambino 
         return albumTracks;                                                    //Return the songs for Childish Gambino in the form of an ArrayList
    }
}
