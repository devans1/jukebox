package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class CoheedAndCambria {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public CoheedAndCambria() {
    }
    
    public ArrayList<Song> getCoheedSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Wake Up", "Coheed and Cambria");             	//Create a song
         Song track2 = new Song("The Island", "Coheed and Cambria");         	//Create another song
         Song track3 = new Song("Welcome Home", "The Coheed and Cambria");      //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Coheed and Cambria
         this.albumTracks.add(track2); 											//Add the second song to song list for Coheed and Cambria 
         this.albumTracks.add(track3); 											//Add the third song to song list for Coheed and Cambria 
         return albumTracks;                                                    //Return the songs for Coheed and Cambria in the form of an ArrayList
    }
}
