package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class IgorMozheyko_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
		//  Create a playlist.
		
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	
		//  Declare and Read songs from Guns N Roses Artist
		
		ArrayList<Song> GunsNRosesTracks = new ArrayList<Song>();		//  Create an ArrayList to hold songs
		GunsNRoses gunsNRoses = new GunsNRoses();						//  Create an instance of the music.artist class
		GunsNRosesTracks = gunsNRoses.getGunsNRosesSongs();				//  Get songs from the artist class and place them
																		//  In the array list
    
		//  Declare and Read songs from Linkin Park Artist
		
		LinkinPark linkinPark = new LinkinPark();						//  Create an ArrayList to hold songs
		ArrayList<Song> LinkinParkTracks = new ArrayList<Song>();		//  Create an instance of the music.artist class
		LinkinParkTracks = linkinPark.getLinkinParkSongs();				//  Get songs from the artist class and place them
																		//  in the array list.
		
		//  Declare and Read songs from Adele Artist
		
		Adele adele = new Adele();					
		ArrayList<Song> AdeleTracks = new ArrayList<Song>();	
		AdeleTracks = adele.getAdelesSongs();		
		
		// Declare and Read songs from Green Day Artist
		
		GreenDay greenDay = new GreenDay();
		ArrayList<Song> GreenDayTracks = new ArrayList<Song>();
		GreenDayTracks = greenDay.getGreenDaySongs();																	
    
		//  Retrieve songs from ArrayList and place them into the playlist
		//  Selected songs are in one plays to alter both songs and artists.
		
		playlist.add(GunsNRosesTracks.get(0));
		playlist.add(LinkinParkTracks.get(1));
		playlist.add(GunsNRosesTracks.get(1));
		playlist.add(LinkinParkTracks.get(1));
		playlist.add(LinkinParkTracks.get(2));
		playlist.add(GunsNRosesTracks.get(2));
		playlist.add(AdeleTracks.get(0));
		playlist.add(GreenDayTracks.get(0));
			
    return playlist;								// Return playlist.
	}
}
